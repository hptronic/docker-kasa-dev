Generovani kontajneru:

```
#!bash

docker-compose build
```


Start:

```
#!bash

docker-compose up

```
Start demoda:

```
#!bash

docker-compose up -d
```

Stop(vsech kontajneru):

```
#!bash

docker-compose stop
```


Pripojeni do konzole:

```
#!bash

docker exec -it dockerkasa_web_1 /bin/bash
```

Spusteni php prikazu:

```
#!bash

docker exec dockerkasa_web_1 php -r 'echo "It works!";'
```